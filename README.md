# TKL data-import-xmlcatalogueitem-extractor
TKL data-push-catalogueitem-collector
TKL data-import-xmlcatalogueitem-extractor provides xml-collectors of catalogueItem from MongoDB, cataloueItem collections.  
Support PSR-1, PSR-2, PSR-3, PSR-4.

![sa24 package repository](https://www.dream-destination-wedding.com/images/exotic2.jpg)

## Install

Via Composer

``` bash
$ composer require tkl/data-push-catalogueitem-collector
```

## Usage

``` bash
$ php
$ 
```

## Testing

``` bash
$ phpunit
```

## Contributing

Please see [CONTRIBUTING](https://github.com/tkl/data-push-catalogueitem-collector/blob/master/CONTRIBUTING.md) for details.

## Credits

- [Wilson Smith](https://github.com/wilsonsmith)
- [All Contributors](https://github.com/tkl/data-push-catalogueitem-collector/contributors)

## License

The GNU General Public Licence version 3 (GPLv3). Please see [License File](LICENSE.md) for more information.
