<?php
namespace DataPushCatalogueItemCollector\tests;

use DataAccessDAL\DAL;
use Dotenv\Dotenv;
use DataPushCatalogueItemCollector\CatalogueItemCollector;

class CatalogueItemCollectorTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $dotenv = new Dotenv(__DIR__);
        $dotenv->load();
        DAL::getInstance()->setCollections(getenv('MONGOCOLLECTIONNAME'), getenv('MONGOCOLLECTIONSA24NAME'));
        try {
            DAL::dropCollection(getenv('MONGOCOLLECTIONSA24NAME'));
        } catch (\Exception $e) {
            echo("The collection doesn't exist.\n");
        }
        DAL::getInstance()->createCollection(getenv('MONGOCOLLECTIONSA24NAME'));
        DAL::getInstance()->createCollection(getenv('MONGOCOLLECTIONNAME'));
    }
    public function tearDown()
    {
        DAL::dropCollection(getenv('MONGOCOLLECTIONSA24NAME'));
        DAL::dropCollection(getenv('MONGOCOLLECTIONNAME'));
    }
    private function insertTestObjects($qty = 10)
    {
        $srv = getenv('MONGOSERVER');
        $port = getenv('MONGOPORT');
        $manager = new \MongoDB\Driver\Manager("mongodb://$srv:$port");
        $bulkGS1 = new \MongoDB\Driver\BulkWrite;
        $bulkSA24 = new \MongoDB\Driver\BulkWrite;
        $writeConcern = new \MongoDB\Driver\WriteConcern(
            \MongoDB\Driver\WriteConcern::MAJORITY,
            1000
        );
        for ($i = 0; $i < $qty; $i++) {
            $item = [
                "catalogueItem" => [
                    "source" => "6G6",
                    "tradeItem" => [
                        "gtin" => "054100768950$i",
                        "ean" => "0001234567$i",
                        "articleNumber" => "348907-$i",
                        "informationProviderOfTradeItem" => [
                            "gln" => "00370000000$i",
                            "vat" => [
                                "cvrNumber" => [
                                    "@value" => "035849$i",
                                    "@attributes" => [
                                        "languageCode" => "da"
                                    ]
                                ]
                            ]
                        ],
                        "tradeItemInformation" => [
                            "extension" => [
                                "trade_item_description:tradeItemDescriptionModule" => [
                                    "tradeItemDescriptionInformation" => [
                                        "descriptionShort" => "PANTENE REPAIR 250".$i."ML",
                                        "brandNameInformation" => [
                                            "brandName" => "Pantene"
                                        ]
                                    ]
                                ],
                                "trade_item_measurements:tradeItemMeasurementsModule" => [
                                    "tradeItemMeasurements" => [
                                        "tradeItemWeight" => [
                                            "netWeight" => [
                                                "@value" => "$i",
                                                "@attributes" => [
                                                    "measurementUnitCode" => "GRM"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                "trade_item_stock_information:tradeItemStockInformationModule" => [
                                    "warehouse" => [
                                        "name" => "noavision",
                                        "rules" => [
                                            "reorderPoint" => "$i",
                                            "minStock" => "$i",
                                            "leadTime" => "$i",
                                            "notificationDays" => "90"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                "push" => [
                    "pending" => true,
                    "last" => ""
                ]
            ];
            $bulkGS1->update(
                ['catalogueItem.tradeItem.gtin' => $item['catalogueItem']['tradeItem']['gtin']],
                ['$set' => $item],
                ['multi' => false, 'upsert' => true]
            );
            $bulkSA24->update(
                ['catalogueItem.tradeItem.gtin' => $item['catalogueItem']['tradeItem']['gtin']],
                ['$set' => $item],
                ['multi' => false, 'upsert' => true]
            );
        }
        
        $manager->executeBulkWrite(getenv('MONGODBNAME').".".getenv('MONGOCOLLECTIONNAME'), $bulkGS1, $writeConcern);
        $manager->executeBulkWrite(
            getenv('MONGODBNAME').".".getenv('MONGOCOLLECTIONSA24NAME'),
            $bulkSA24,
            $writeConcern
        );
    }
    public function testItemCollector()
    {
        $this->insertTestObjects(10);
        $cic = new CatalogueItemCollector();
        $result = $cic->collect();
        $this->assertTrue(count($result['SA24']) === 10);
        $this->assertTrue(count($result['GS1']) === 10);
    }
    public function testEmptyItemCollector()
    {
        $cic = new CatalogueItemCollector();
        $result = $cic->collect();
        $this->assertFalse($result);
    }
}
