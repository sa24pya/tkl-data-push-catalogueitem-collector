<?php
/**
 * File: Collector.php
 *
 * @author Jesus Coll Cerdan jesus.coll@basetis.com, bcn
 * @version 1.0
 * @package application
 */
namespace DataPushCatalogueItemCollector;

use DataAccessDAL\DAL;
use Core\Tool\Logging;

/**
 * Class Collector
 * It reads products information in a
 * mongo database storage.
 * Select product marked as to be pushed.
 * @package application
 * @subpackage DataAccess
 */
class CatalogueItemCollector
{
    private $logger = null;
    /**
     * Database Access Layer
     * @var object
     */
    private $dal ;
    
    public static function collect()
    {
        try {
            $dal = DAL::getInstance();
            $gtin = [];

            //Query filters, we need items to push
            $catalogue =[
                'push.pending' => true
            ];

            // only gtin id is required
            $fields = [
                 'catalogueItem.tradeItem.gtin'
            ];

            // TODO: remove next print_r, only for firsts test

            //print_r($result['SA24']);

            $result = $dal->catalogueItemGetGtin4Push();

            if (( !empty($result['SA24']) || !empty($result['GS1']))) {
                foreach ($result['SA24'] as $catalogueItem) {
                    $gtin[] = $catalogueItem->tradeItem->gtin;
                }
                if (!empty($result['GS1'])) {
                    foreach ($result['GS1'] as $catalogueItem) {
                        $gtin[] = $catalogueItem->tradeItem->gtin;
                    }
                    $gtin = array_merge(array_flip(array_flip($gtin)));
                }

                //GS1 & SA24 fields
                $tradeItem = 'catalogueItem.tradeItem';
                $extension_path = "$tradeItem.tradeItemInformation.extension";
                $additional = "additionalTradeItemIdentification";
                // Extensions:
                $description = "trade_item_description:tradeItemDescriptionModule.tradeItemDescriptionInformation";
                $dutyFree = "duty_fee_tax_information:dutyFeeTaxInformationModule";
                $food = "food_and_beverage_ingredient:foodAndBeverageIngredientModule";
                $measurements = "trade_item_measurements:tradeItemMeasurementsModule.tradeItemMeasurements";
                $referencedItem = "referenced_file_detail_information:referencedFileDetailInformationModule";
                $sales = "sales_information:salesInformationModule.salesInformation";
                $temperature = "trade_item_temperature_information:tradeItemTemperatureInformationModule";
                $warehouse = "$extension_path.trade_item_stock_information:tradeItemStockInformationModule.warehouse";
                // Fields:
                $fields = [
                    "$tradeItem.gtin",
                    "$tradeItem.ean",
                    "$tradeItem.informationProviderOfTradeItem.gln",
                    "$tradeItem.informationProviderOfTradeItem.vat.cvrNumber.@value",
                    "$tradeItem.informationProviderOfTradeItem.vat.cvrNumber.@attributes.languageCode",
                    "$tradeItem.$additional.@value",
                    "$tradeItem.$additional.@attributes.additionalTradeItemIdentificationTypeCode",
                    "$extension_path.$description.descriptionShort",
                    "$extension_path.$measurements.tradeItemWeight.netWeight.@value",
                    "$extension_path.$measurements.".
                    "tradeItemWeight.netWeight.@attributes.measurementUnitCode",
                    "$extension_path.$measurements.tradeItemWeight.grossWeight",
                    "catalogueItem.tradeItem.informationProviderOfTradeItem.partyName",
                    "$extension_path.$description.brandNameInformation.brandName",
                    "$extension_path.$description.functionalName.@value",
                    "catalogueItem.tradeItem.isTradeItemAConsumerUnit",
                    "$extension_path.packaging_marking:packagingMarkingModule.".
                    "packagingMarking.isPackagingMarkedReturnable",
                    "$warehouse.rules.minStock",
                    "$warehouse.rules.leadTime",
                    "$warehouse.rules.notificationDays",
                    "$tradeItem.manufacturerOfTradeItem.gln",
                    "$tradeItem.targetMarket.targetMarketCountryCode",
                    "$extension_path.$dutyFree.dutyFeeTaxInformation.dutyFeeTaxAgencyCode",
                    "$extension_path.allergen_information:AllergenInformationModule.allergenStatement",
                    "$extension_path.$measurements.depth.@value",
                    "$extension_path.$measurements.depth.@attributes.measurementUnitCode",
                    "$extension_path.$measurements.height.@value",
                    "$extension_path.$measurements.height.@attributes.measurementUnitCode",
                    "$extension_path.$measurements.width.@value",
                    "$extension_path.$measurements.width.@attributes.measurementUnitCode",
                    "$extension_path.$measurements.netContent.@value",
                    "$extension_path.$measurements.netContent.@attributes.measurementUnitCode",
                    "$extension_path.$food.ingredientStatement.@value",
                    "$extension_path.packaging_marking:packagingMarkingModule.packagingMarking.isPriceOnPack",
                    "$extension_path.$temperature.tradeItemTemperatureInformation.minimumTemperature.@value",
                    "$extension_path.$temperature.tradeItemTemperatureInformation.minimumTemperature.@attributes.".
                    "temperatureMeasurementUnitCode",
                    "$extension_path.$temperature.tradeItemTemperatureInformation.maximumTemperature.@value",
                    "$extension_path.$temperature.tradeItemTemperatureInformation.maximumTemperature.@attributes.".
                    "temperatureMeasurementUnitCode",
                    "$extension_path.packaging_information:packagingInformationModule.packaging.packagingTypeCode",
                    "$extension_path.$sales.priceComparisonMeasurement.@value",
                    "$extension_path.$referencedItem.referencedFileHeader.uniformResourceIdentifier",
                ];

                $result = $dal->catalogueItemRead4Push(['filters' => $gtin, 'fields' => $fields]);

                return $result;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            // write message to the log file
            $log = new Logging();
            $log->lfile('Error.log');
            $log->lwrite('collect: '.$e->getMessage().'');
            $log->lclose();
            return false;
        }
    }
}
